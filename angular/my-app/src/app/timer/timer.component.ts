import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  minutes: number = 0;
  seconds: number = 0;
  isPause: boolean = true;
  buttonLabel: string = 'Empezar';

  constructor() {
    this.reset();
    setInterval(() => this.tick(), 1000);
  }

  reset() {
    this.minutes = 24;
    this.seconds = 59;
  }

  private tick() {
    if(!this.isPause) {
      this.buttonLabel = 'Pausar';
      if (--this.seconds < 0) {
        this.seconds = 59;
        if (--this.minutes < 0) {
          this.reset();
        }
      }
    }
  }

  pause() {
    this.isPause = !this.isPause;
    if(this.minutes <24 || this.seconds < 59) {
      this.buttonLabel = this.isPause ? 'Reanudar':'Pausar';
    }
  }

  ngOnInit(): void {
  }

}
