import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-holamundo',
  templateUrl: './holamundo.component.html',
  styleUrls: ['./holamundo.component.css']
})
export class HolamundoComponent implements OnInit {
  texto: string = 'Estoy mostrando algo ;)';
  show: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }

  public cambiarVisibilidad(): void {
    this.show = !this.show;
  }
}
