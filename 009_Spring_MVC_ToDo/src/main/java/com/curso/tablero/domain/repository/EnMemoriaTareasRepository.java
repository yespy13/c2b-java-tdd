package com.curso.tablero.domain.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.curso.tablero.domain.Estados;
import com.curso.tablero.domain.Tarea;
import com.curso.tablero.exceptions.TareasException;

@Repository
public class EnMemoriaTareasRepository implements TareasRepository {
	private Map<Integer, Tarea> tareas = new HashMap<Integer, Tarea>();
	private static int contadorId = 0;
	
	public EnMemoriaTareasRepository() {
		tareas.put(1, new Tarea(1, "Tarea 1","Lo que sea"));
        tareas.put(2, new Tarea(2, "Tarea 2","Lo que sea 2"));
        Tarea t3 = new Tarea(3, "Tarea 3","Lo que sea 3");
        Tarea t4 = new Tarea(4, "Tarea 4","Lo que sea 4");
        Tarea t5 = new Tarea(5, "Tarea 5","Lo que sea 5");
        Tarea t6 = new Tarea(6, "Tarea 6","Lo que sea 6");
        t3.setEstado(Estados.DOING);
        t4.setEstado(Estados.DOING);
        t5.setEstado(Estados.DOING);
        t6.setEstado(Estados.DONE);
        tareas.put(3, t3);
        tareas.put(4, t4);
        tareas.put(5, t5);
        tareas.put(6, t6);
    }

	@Override
	public Tarea create(Tarea tarea) {
		contadorId++;
		tarea.setId(contadorId);
		return tarea;
	}
	
	@Override
	public Tarea getTareaById(Integer id) {
		return tareas.get(id);
	}

	@Override
	public Collection<Tarea> getTareasByEstado(Estados estado) {
		List<Tarea> lista = new ArrayList<Tarea>();
		for(Tarea t : tareas.values()) {
			if(t.getEstado() == estado) {
				lista.add(t);
			}
		}
		return lista;
	}

	@Override
	public void update(Tarea tarea) throws TareasException {
		if(!tareas.containsKey(tarea.getId())) {
			throw new TareasException("La tarea a modificar no existe");
		}
		tareas.put(tarea.getId(), tarea);
	}

	@Override
	public void delete(Integer id) throws TareasException {
		Tarea tarea = getTareaById(id);
		if(tarea == null) {
			throw new TareasException("La tarea a borrar no existe");
		}
		tareas.remove(id);
	}

}
