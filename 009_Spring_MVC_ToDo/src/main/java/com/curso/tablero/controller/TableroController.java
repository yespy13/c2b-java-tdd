package com.curso.tablero.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.curso.tablero.domain.Tarea;
import com.curso.tablero.exceptions.TareasException;
import com.curso.tablero.services.TableroService;

@Controller
public class TableroController {
	@Autowired
	private TableroService tableroService;
	
	// get	/tablero
	@RequestMapping("/tablero")
	public String mostrarTablero(Model model) {
		model.addAttribute("columnaToDo", tableroService.getTareasToDo());
		model.addAttribute("columnaDoing", tableroService.getTareasDoing());
		model.addAttribute("columnaDone", tableroService.getTareasDone());
		return "tablero";
	}
	// get 	/tablero/nueva tarea
	@GetMapping("/tablero/{id}/{nuevoestado}")
	public String cambiarEstado(@PathVariable("id") int id, @PathVariable("nuevoestado") String nuevoEstado) throws TareasException {
		switch(nuevoEstado) {
			case "TODO":
				tableroService.moverAToDo(id);
				break;
			case "DOING":
				tableroService.moverADoing(id);
				break;
			case "DONE":
				tableroService.moverADone(id);
				break;
			default:
				throw new TareasException("No ha indicado un estado válido.");
				
		}
		return "redirect:/tablero";
	}

	@GetMapping("/tablero/nueva-tarea")
	public String getCrearNuevaTareaFormulario(Model model) {
		Tarea tarea = new Tarea();
		model.addAttribute("nuevaTarea", tarea);
		return "nueva-tarea";
	}
	
	@PostMapping("/tablero/nueva-tarea")
	public String crearNuevaTarea(@ModelAttribute("nuevaTarea") Tarea nueva, Model model) throws TareasException {
		tableroService.crearTarea(nueva);
		return "redirect:/tablero";
	}
	
	@ExceptionHandler(TareasException.class)
    public ModelAndView handleException( TareasException exception) {
         ModelAndView mav = new ModelAndView();

         mav.addObject("claveMensage",exception.getKeyMensaje());
         mav.addObject("argsMensage", exception.getArgs());
         exception.printStackTrace();
         mav.setViewName("tareas-exception");
         return mav;
    }
}
