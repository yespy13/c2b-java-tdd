
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet"
              href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
        <title>Tablero kanban</title>
    </head>
    <body>
        <section>
            <div class="jumbotron">
                <div class="container">
                    <h1>Tablero Kanban</h1>
                    <p>Lista de tareas</p>
                </div>
            </div>
        </section>

        <section class="container">
        	<a class="btn btn-primary" href="tablero/nueva-tarea">Nueva tarea</a>
            <div class="row">
            	<div class="col">
	                <h2>To Do</h2>
	                <c:forEach items="${columnaToDo}" var="tarea">
	                    <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
	                        <div class="thumbnail">
	                            <div class="caption">
	                                <h3>${tarea.titulo}</h3>
	                                <p>${tarea.descripcion}</p>
	                            </div>
	                        </div>
	                    </div>
	                </c:forEach>
                </div>
			</div>
			<div class="row">
				<div class="col">
	                <h2>Doing</h2>
	                <c:forEach items="${columnaDoing}" var="tarea">
	                    <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
	                        <div class="thumbnail">
	                            <div class="caption">
	                                <h3>${tarea.titulo}</h3>
	                                <p>${tarea.descripcion}</p>
	                            </div>
	                        </div>
	                    </div>
	                </c:forEach>
                </div>
			</div>
			<div class="row">
				<div class="col">
	                <h2>Done</h2>
	                <c:forEach items="${columnaDone}" var="tarea">
	                    <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
	                        <div class="thumbnail">
	                            <div class="caption">
	                                <h3>${tarea.titulo}</h3>
	                                <p>${tarea.descripcion}</p>
	                            </div>
	                        </div>
	                    </div>
                	</c:forEach>
               	</div>
            </div> 
        </section>
    </body>
</html>