package com.curso.juego;

public class TresEnRaya {
	private char[][] tablero = new char[3][3];
	private char ficha = 'X';
	private int win = 0;

	public void jugar(int x, int y) {
		x--;
		y--;
		checkCoords(x, y);
		ponerFicha(x, y);
		win = checkWin(x, y);
	}

	public boolean checkCoords(int x, int y) {
		boolean valid = true;
		if(x < 0 || x > 2 || y < 0 || y > 2) {
			valid = false;
			throw new RuntimeException("Coordenada fuera de rango.");
		} else if (!checkOcupado(x, y)) {
			valid = false;
		}
		return valid;
	}

	public boolean checkOcupado(int x, int y) {
		boolean valid = true;
		if((tablero[x][y] != '\0')) {
			valid = false;
			throw new RuntimeException("Casilla ocupada.");
		}
		return valid;
	}

	public void ponerFicha(int x, int y) {
		tablero[x][y] = this.ficha;
		cambiarFicha();
	}

	private void cambiarFicha() {
		if(this.ficha == 'X') {
			this.ficha = 'O';
		} else if(this.ficha == 'O') {
			this.ficha = 'X';
		}
	}

	public void mostrarTablero() {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(j == 1)
					System.out.printf(" | ");
				System.out.print(tablero[i][j]);
				if(j == 1)
					System.out.printf(" | ");					
			}
			if(i != 2) {
				System.out.print("\n");
				System.out.println("---------");
			}
		}
	}

	public char getCasilla(int x, int y) {
		return tablero[x-1][y-1];
	}

	public int checkWin(int x, int y) {
		win = checkLinea(x);
		if(win != 1 && win != 2)
			win = checkColumna(y);
		if(win != 1 && win != 2)
			win = checkDiagonal(x, y);
		if(win != 1 && win != 2)
			win = checkEmpate(x, y);
		return win;
	}

	public int checkLinea(int x) {
		if(tablero[x][0] == 'X' && tablero[x][1] == 'X' && tablero[x][2] == 'X') {
			return 1;
		} else if(tablero[x][0] == 'O' && tablero[x][1] == 'O' && tablero[x][2] == 'O') {
			return 2;
		}
		return 3;
	}

	public int checkColumna(int y) {
		if(tablero[0][y] == 'X' && tablero[1][y] == 'X' && tablero[2][y] == 'X') {
			return 1;
		} else if(tablero[0][y] == 'O' && tablero[1][y] == 'O' && tablero[2][y] == 'O') {
			return 2;
		}
		return 3;
	}

	private int checkDiagonal(int x, int y) {
		if(tablero[0][0] == 'X' && tablero[1][1] == 'X' && tablero[2][2] == 'X') {
			return 1;
		} else if(tablero[0][2] == 'X' && tablero[1][1] == 'X' && tablero[2][0] == 'X') {
			return 1;
		} else if(tablero[0][0] == 'O' && tablero[1][1] == 'O' && tablero[2][2] == 'O') {
			return 2;
		} else if(tablero[0][2] == 'O' && tablero[1][1] == 'O' && tablero[2][0] == 'O') {
			return 2;
		}
		return 0;
	}

	private int checkEmpate(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}
}
