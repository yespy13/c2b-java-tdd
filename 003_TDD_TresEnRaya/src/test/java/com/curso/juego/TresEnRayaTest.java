package com.curso.juego;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TresEnRayaTest {
	private TresEnRaya ter;
	
	@Before
	public void prepararPrueba() {
		this.ter = new TresEnRaya();
	}

	@Test(expected = RuntimeException.class)
	public void whenCoordXFueraDelTableroRuntimeException() throws RuntimeException {
		ter.jugar(5, 2);
	}
	
	@Test(expected = RuntimeException.class)
	public void whenCoordYFueraDelTableroRuntimeException() throws RuntimeException {
		ter.jugar(2, 5);
	}
	
	@Test(expected = RuntimeException.class)
	public void casillaOcupada() throws RuntimeException {
		ter.jugar(3, 2);
		ter.jugar(3, 2);
	}
	
	@Test
	public void casillaCorrectaYFichaColocada() {
		char expected = 'X';
		
		ter.jugar(2, 1);
		
		char resultado = ter.getCasilla(2, 1);
		
		if(expected != resultado) {
			fail("Esperaba X en la posición 2, 1 pero se ha recibido" + resultado);
		}
	}
	
	@Test
	public void cambiaJugadorCorrectamente() {	
		char expected = 'X';
		
		ter.jugar(2, 1);
		
		char resultado = ter.getCasilla(2, 1);
		
		if(expected != resultado) {
			fail("Esperaba X en la posición 2, 1 pero se ha recibido " + resultado);
		}		
		
		expected = 'O';
		
		ter.jugar(3, 2);
		
		resultado = ter.getCasilla(3, 2);
		
		if(expected != resultado) {
			fail("Esperaba O en la posición 3, 2 pero se ha recibido " + resultado);
		}		
		
		expected = 'X';
		
		ter.jugar(1, 3);
		
		resultado = ter.getCasilla(1, 3);
		
		if(expected != resultado) {
			fail("Esperaba X en la posición 1, 3 pero se ha recibido " + resultado);
		}		
		
		expected = 'O';
		
		ter.jugar(1, 1);
		
		resultado = ter.getCasilla(1, 1);
		
		if(expected != resultado) {
			fail("Esperaba O en la posición 1, 1 pero se ha recibido " + resultado);
		}
	}
	
	@Test
	public void comprobarGanarLineaJugador1() {
		ter.jugar(1, 1);
		ter.jugar(2, 1);
		ter.jugar(1, 2);
		ter.jugar(2, 2);
		ter.jugar(1, 3);
		assertTrue(ter.checkWin(0, 2) == 1);
	}
	
	@Test
	public void comprobarGanarLineaJugador2() {
		ter.jugar(1, 1);
		ter.jugar(2, 1);
		ter.jugar(1, 2);
		ter.jugar(2, 2);
		ter.jugar(3, 1);
		ter.jugar(2, 3);
		assertTrue(ter.checkWin(1, 0) == 2);
	}
	
	@Test
	public void comprobarGanarColumnaJugador1() {
		ter.jugar(1, 1);
		ter.jugar(2, 2);
		ter.jugar(2, 1);
		ter.jugar(2, 3);
		ter.jugar(3, 1);
		assertTrue(ter.checkWin(0, 0) == 1);
	}
	
	@Test
	public void comprobarGanarColumnaJugador2() {
		ter.jugar(1, 1);
		ter.jugar(1, 2);
		ter.jugar(1, 3);
		ter.jugar(2, 2);
		ter.jugar(3, 3);
		ter.jugar(3, 2);
		assertTrue(ter.checkWin(1, 1) == 2);
	}
	
	@Test
	public void comprobarGanarDiagonalJugador1() {
		ter.jugar(1, 1);
		ter.jugar(2, 3);
		ter.jugar(2, 2);
		ter.jugar(2, 1);
		ter.jugar(3, 3);
		assertTrue(ter.checkWin(2, 2) == 1);
	}
	
	@Test
	public void comprobarGanarDiagonalJugador2() {
		ter.jugar(1, 2);
		ter.jugar(1, 1);
		ter.jugar(1, 3);
		ter.jugar(2, 2);
		ter.jugar(3, 2);
		ter.jugar(3, 3);
		assertTrue(ter.checkWin(1, 1) == 2);
	}
	
	@Test
	public void comprobarEmpate() {
		ter.jugar(0, 0);
		ter.jugar(0, 1);
		ter.jugar(0, 2);
		ter.jugar(1, 0);
		ter.jugar(1, 1);
		ter.jugar(1, 2);
		ter.jugar(2, 0);
		ter.jugar(2, 1);
		ter.jugar(2, 2);
		assertTrue(ter.checkWin(1, 1) == 3);
	}
}
