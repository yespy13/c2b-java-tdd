package com.curso.pruebas;

import com.curso.modelo.Animal;
import com.curso.modelo.Cat;
import com.curso.modelo.Fish;
import com.curso.modelo.Pet;
import com.curso.modelo.Spider;

public class TestAnimals {

	public static void main(String[] args) {
		Fish d = new Fish();
		Cat c = new Cat("Fluffy");
		Animal a = new Fish();
		Animal e = new Spider();
		Pet p = new Cat("Carpacho");
		
		d.eat();
		d.walk();
		c.walk();
		c.eat();
		a.walk();
		a.eat();
		e.walk();
		System.out.println(p.getName());
	}

}
