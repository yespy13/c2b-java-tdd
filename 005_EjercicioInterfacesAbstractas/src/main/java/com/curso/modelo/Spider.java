package com.curso.modelo;

public class Spider extends Animal {
	
	public Spider() {
		super(8);
	}

	@Override
	public void eat() {
		System.out.println("Soy un araña que come cosas que comen las arañas.");
	}

}
