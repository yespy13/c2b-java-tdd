package com.curso.modelo;

public interface Pet {
	public String getName();
	public void setName(String name);
	public void play();
}
