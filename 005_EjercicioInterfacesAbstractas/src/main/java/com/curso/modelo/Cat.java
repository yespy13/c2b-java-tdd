package com.curso.modelo;

public class Cat extends Animal implements Pet {
	private String name;

	public Cat(String name) {
		super(4);
		this.name = name;
	}
	
	public Cat() {
		super(4);
		this.name = "";
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void play() {
		System.out.println("Juego con mi gato.");
	}

	@Override
	public void eat() {
		System.out.println("Mi gato come comida de gatos.");
	}

}
