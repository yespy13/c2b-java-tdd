package com.curso.modelo;

public class Fish extends Animal implements Pet {
	private String name;

	public Fish() {
		super(0);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void play() {
		System.out.println("Juego con mi pez, no sé cómo...");
	}

	@Override
	public void walk() {
		System.out.println("El pez no anda, si no que nada");
	}

	@Override
	public void eat() {
		System.out.println("El pez come como unas papelinas raras.");
	}

}
