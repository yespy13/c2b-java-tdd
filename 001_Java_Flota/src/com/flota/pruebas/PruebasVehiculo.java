package com.flota.pruebas;

import com.flota.modelo.Caja;
import com.flota.modelo.Vehiculo;

public class PruebasVehiculo {
	
	public static void main(String[] args) {
		Vehiculo v1 = new Vehiculo("1127CCL");
		
		v1.addCaja(new Caja());
		
		System.out.println("Carga actual: " + v1.getCargaActual() + "; Carga m�xima: " + v1.getCargaMax());

		System.out.println(v1.addCaja(new Caja()));
		
		System.out.println("Carga actual: " + v1.getCargaActual() + "; Carga m�xima: " + v1.getCargaMax());
		
		System.out.println(v1.addCaja(new Caja(73.56)));
		
		System.out.println("Carga actual: " + v1.getCargaActual() + "; Carga m�xima: " + v1.getCargaMax());
		
		System.out.println(v1.addCaja(new Caja(875)));			
		
		System.out.println("Carga actual: " + v1.getCargaActual() + "; Carga m�xima: " + v1.getCargaMax());
	}
}
