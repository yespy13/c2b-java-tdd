package com.flota.modelo;

public class Caja {
	private double peso;

	public Caja() {
		super();
		this.peso = 10;
	}

	public Caja(double peso) {
		super();
		this.peso = peso;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
}
