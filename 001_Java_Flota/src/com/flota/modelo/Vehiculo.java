package com.flota.modelo;

public abstract class Vehiculo {
	private String matricula;
	private double cargaMax;
	private double cargaActual;
	private int numCajas;
	
	public Vehiculo(String matricula, double cargaMax) {
		super();
		this.matricula = matricula;
		this.cargaMax = cargaMax;
	}	

	public Vehiculo(String matricula) {
		super();
		this.matricula = matricula;
		this.cargaMax = 3000;
	}



	public double getCargaMax() {
		return cargaMax;
	}
	
	public void setCargaMax(double cargaMax) {
		this.cargaMax = cargaMax;
	}
	
	public String getMatricula() {
		return matricula;
	}

	public double getCargaActual() {
		return cargaActual;
	}

	public int getNumCajas() {
		return numCajas;
	}
	
	public abstract double calcularEficienciaFuel();

	/***
	 * Carga una nueva caja en el veh�culo.
	 * @param c objeto de la clase Caja.
	 * @return true o false dependiendo de si hace la carga o no
	 */
	public boolean addCaja(Caja c) {
		if ((c.getPeso() >= 0) && (cargaActual + c.getPeso() <= cargaMax)) {
			this.cargaActual += c.getPeso();
			this.numCajas ++;
			return true;
		} else {
			System.out.println("El peso de la caja (" + c.getPeso() + ") no puede hacer que la carga actual (" + this.cargaActual + ") la carga m�xima (" + this.cargaMax + ").");
			return false;
		}
	}

}
