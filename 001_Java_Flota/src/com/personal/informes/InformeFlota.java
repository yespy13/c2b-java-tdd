package com.personal.informes;

import com.flota.modelo.Vehiculo;

public class InformeFlota {
	public void generaInformeConsumoFuel(Vehiculo[] flota) {
		double consumoTotal = 0;
		double consumoVehiculo = 0;
		for(Vehiculo v : flota) {
			consumoVehiculo = v.calcularEficienciaFuel();
			consumoTotal += consumoVehiculo;
			System.out.println(v.getMatricula() + ": " + consumoVehiculo + " litros.");
		}
		System.out.println("Total: " + consumoTotal);
	}
	
	public static void main(String[] args) {
		Vehiculo[] flota = {
				new Camion("A2882"),
				new Barcaza("B7778"),
				new Barcaza("B9827")
		}
	}
}
