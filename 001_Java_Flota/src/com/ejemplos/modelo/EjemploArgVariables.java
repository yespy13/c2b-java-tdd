package com.ejemplos.modelo;

public class EjemploArgVariables {
	public static double calcularMedia(int... numeros) {
		double r = 0;
		for(int n:numeros) {
			r += n;
		}
		
		r = r/numeros.length;
		return r;
	}
	
	public static void main(String[] args) {
		int[] datos = {3, 4, 5, 6, 7};
		
		System.out.println("Media: " + calcularMedia(3, 4, 5, 6, 7));
 	}
}
