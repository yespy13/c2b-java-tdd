package com.curso.core.practica2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MiAplicacion {

	public static void main(String[] args) {
		
		/**
		 * Sin Spring
		 */
		/*RRHHService service = new RRHHService();
		
		Solicitud s1 = new SolicitudVacaciones();
		
		service.gestionarSolicitud(s1);*/
		
		/**
		 * CON Spring
		 */
		ApplicationContext ctx = new ClassPathXmlApplicationContext("/com/curso/practica1/solicitudes-beans.xml");
		
	}

}
