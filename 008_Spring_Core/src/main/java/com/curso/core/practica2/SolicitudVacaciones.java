package com.curso.core.practica2;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

@Component("SolicitudLibre")
public class SolicitudVacaciones extends Solicitud {
	private String departamento;
	
	public SolicitudVacaciones(String departamento) {
		this.departamento = departamento;
	}

	@Override
	public void aprobar() {
		this.fechaRevision = LocalDate.now();
		if(this.departamento.equals("VENTAS")) {
			this.aceptada = true;
			System.out.println("Vacaciones aceptadas");			
		} else {
			this.aceptada = false;
			System.out.println("Vacaciones rechazadas");
		}
	}
	
}
