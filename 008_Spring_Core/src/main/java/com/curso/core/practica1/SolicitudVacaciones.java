package com.curso.core.practica1;

import java.time.LocalDate;

public class SolicitudVacaciones extends Solicitud {
	private String departamento;
	
	public SolicitudVacaciones(String departamento) {
		this.departamento = departamento;
	}

	@Override
	public void aprobar() {
		this.fechaRevision = LocalDate.now();
		if(this.departamento.equals("VENTAS")) {
			this.aceptada = true;
			System.out.println("Vacaciones aceptadas");			
		} else {
			this.aceptada = false;
			System.out.println("Vacaciones rechazadas");
		}
	}
	
}
