package com.spring.ejem02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author PROGRAMIA
 */
public class JefeEquipo  implements Gestor{

    private int equipo;
    
    @Autowired
    @Qualifier("progJava")
    private Recurso recurso;

    public JefeEquipo() {
        equipo= 1;
    }

    public JefeEquipo(int equipo, Recurso recurso) {
        this.equipo = equipo;
        this.recurso = recurso;
    }
    

    public void gestionar() {
        System.out.println("GESTIONO EL EQUIPO " + equipo );
        System.out.println("Iniciar Recursos");
        recurso.trabajar();
    }

}
