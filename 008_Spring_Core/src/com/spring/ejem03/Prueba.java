package com.spring.ejem03;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * CREACION DE UN BEAN EN SPRING CONTEXT
 * USANDO ANOTACION @Resource
 * @author PROGRAMIA
 */
public class Prueba {


    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("com/spring/ejem03/gestion.xml");
        Gestor servicio = (Gestor) context.getBean("jefe1");
        servicio.gestionar();
    }

}
