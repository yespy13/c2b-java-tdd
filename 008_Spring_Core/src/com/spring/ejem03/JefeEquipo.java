package com.spring.ejem03;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 *
 * @author PROGRAMIA
 */
public class JefeEquipo  implements Gestor{

    
    private int equipo;
    
    @Resource(name = "prog2")
    private Recurso recurso;

    public JefeEquipo() {
        equipo= 1;
        
        //todavia recurso es null
    }
    
    @PostConstruct
    public void iniciar(){
        // ya no es null 
    }
    

    public JefeEquipo(int equipo, Recurso recurso) {
        this.equipo = equipo;
        this.recurso = recurso;
    }
    

    public void gestionar() {
        System.out.println("GESTIONO EL EQUIPO " + equipo );
        System.out.println("Iniciar Recursos");
        recurso.trabajar();
    }
    
    

}
