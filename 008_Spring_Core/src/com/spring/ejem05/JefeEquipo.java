
package com.spring.ejem05;

import javax.annotation.Resource;

/**
 *
 * @author PROGRAMIA
 */
public class JefeEquipo  implements Gestor{

    private int equipo;
    
    @Resource(name="programador2")
    private Recurso recurso;

    public JefeEquipo() {
        equipo= 1;
    }

    public JefeEquipo(int equipo, Recurso recurso) {
        this.equipo = equipo;
        this.recurso = recurso;
    }
    

    public void gestionar() {
        System.out.println("GESTIONO EL EQUIPO " + equipo );
        System.out.println("Iniciar Recursos");
        recurso.trabajar();
    }

}
