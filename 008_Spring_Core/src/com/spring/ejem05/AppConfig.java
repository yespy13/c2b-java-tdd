
package com.spring.ejem05;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * configuración de los bean de spring mediante programación Java 
 * @author PROGRAMIA
 */
@Configuration
public class AppConfig {
    
    @Bean
    public JefeEquipo  jefeEquipo(){
        return new JefeEquipo();
    }
    
    @Bean 
    public Programador programador1(){
       Programador p= new Programador();
       p.setTarea("Programar en C++");
       return p;
    }
    
    @Bean 
    public Programador programador2(){
       Programador p= new Programador();
       p.setTarea("Programar en Java EE");
       return p;
    }
    
    
}
