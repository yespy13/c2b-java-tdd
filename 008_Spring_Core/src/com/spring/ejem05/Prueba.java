package com.spring.ejem05;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;



/**
 * CREACION DE UN BEAN EN SPRING CONTEXT
 * USANDO programación Java 
 * @author PROGRAMIA
 */
public class Prueba {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        Gestor gestor = (Gestor) ctx.getBean("jefeEquipo");
        gestor.gestionar();
    }

}
