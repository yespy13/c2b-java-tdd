package com.spring.ejem04;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * CREACION DE UN BEAN EN SPRING CONTEXT
 * USANDO  AUTOSCANEO 
 * @author PROGRAMIA
 */
public class Prueba {


    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("com/spring/ejem04/servicios.xml");
        GestionPedidosService servicio = (GestionPedidosService) context.getBean("gestionPedidosABC");
        servicio.gestionar();
    }

}
