
package com.spring.ejem04;

import org.springframework.stereotype.Component;


/**
 *
 * @author bolea
 */
@Component
public class Pedido {

    public Pedido() {   
        System.out.println("... creando pedido");

    }
    
    public void pidiento(){
        System.out.println(".... pidiendo");
    }
 
}
