
package com.spring.ejem04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bolea
 */
@Service("gestionPedidosABC")
public class GestionPedidosServiceImp implements GestionPedidosService {
    
    @Autowired
    private Pedido pedido;
    
    @Override
    public void gestionar(){
        System.out.println("...... GESTIONANDO...");
        pedido.pidiento();
    }
    
}
