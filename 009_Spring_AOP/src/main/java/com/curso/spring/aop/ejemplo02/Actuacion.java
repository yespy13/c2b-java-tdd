
package com.curso.spring.aop.ejemplo02;

public interface Actuacion {

    void actuar() throws Exception;

}
