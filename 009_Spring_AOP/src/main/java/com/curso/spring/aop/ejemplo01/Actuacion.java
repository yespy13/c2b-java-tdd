
package com.curso.spring.aop.ejemplo01;

public interface Actuacion {

    void actuar() throws Exception;

}
