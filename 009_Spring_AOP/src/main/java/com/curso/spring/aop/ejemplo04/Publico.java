package com.curso.spring.aop.ejemplo04;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect

public class Publico {
	@Before("execution(* *.actuar(..))")
    public void tomarAsiento(){
		System.out.println("El público toma asiento");
	}

	@AfterReturning("execution(* *.actuar(..))")
	public void aplaudir(){
		System.out.println("pla pla pla !! Bravo");
	}    

	@AfterThrowing("execution(* *.actuar(..))")
	public void abuchear(){
		System.out.println("Buuuu !! Fuera !!");
	}
}
