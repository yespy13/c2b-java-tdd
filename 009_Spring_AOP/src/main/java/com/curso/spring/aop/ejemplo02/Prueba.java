package com.curso.spring.aop.ejemplo02;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Prueba {
    public static void main(String[] args) {
        try {
            ApplicationContext ctx = new ClassPathXmlApplicationContext("/com/curso/spring/aop/ejemplo02/aop-beans.xml");
            Actuacion a = (Actuacion) ctx.getBean("violinista");
            a.actuar();
            Actuacion a2 = (Actuacion) ctx.getBean("pianista");
            a2.actuar();
        } catch (Exception ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
