package com.curso.spring.aop.ejemplo02;

public class Concertista  implements  Actuacion{

    private String obra;
    private Publico publico;

    public Concertista() {  }

    public void setObra(String obra) {
        this.obra = obra;
    }
    
    public void setPublico(Publico publico) {
		this.publico = publico;
	}

	public void actuar() throws Exception {
        if ( ! obra.equals("desafina")){
             System.out.println("Toco la obra " + obra);
             publico.aplaudir();
        }else{
            System.out.println("Toca una obra desafinada");
            publico.abuchear();
            throw new Exception("El concertista desafina");
        }
    }
}
