
package com.curso.spring.aop.ejemplo03;

public interface Actuacion {

    void actuar() throws Exception;

}
