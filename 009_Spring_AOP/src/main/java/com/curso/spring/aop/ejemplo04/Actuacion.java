
package com.curso.spring.aop.ejemplo04;

public interface Actuacion {

    void actuar() throws Exception;

}
