package com.maquinarefrescos.modelo;

import java.util.ArrayList;

public class MaquinaRefrescos {
	private ArrayList<Refresco> refrescos;
	private double dinero;
	private double dineroInicial;
	
	public MaquinaRefrescos(double dinero) {
		super();
		this.dinero = dinero;
		this.dineroInicial = dinero;
		this.refrescos = new ArrayList<Refresco>(10);
	}
	
	public void getRecaudacion() {
		System.out.println("Hemos obtenido una recaudación de " + (this.dinero - this.dineroInicial) + "€.");
	}

	public ArrayList<Refresco> getRefrescos() {
		return refrescos;
	}
	
	
}
