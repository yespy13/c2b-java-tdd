package com.maquinarefrescos.modelo;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {
	static MaquinaRefrescos m;

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		m = new MaquinaRefrescos(30);
		int op = 0;
		
		cargarRefrescos();
		do {
			System.out.println("Elija una opción: ");
			System.out.println("1 - Consultar precio de un producto.");
			System.out.println("2 - Comprar refresco.");
			System.out.println("3 - Salir.");
			try {
				op = sc.nextInt();
			} catch(Exception e) {
				System.out.println("Error.");
				sc.nextLine();
			}
			if(op == 1) {
				consultarPrecio();
			} else if (op == 2) {
				comprarRefresco();
			} else if (op == 3) {
				System.out.println("Saliendo...");
			} 
		} while(op != 3);
	}

	public static void cargarRefrescos() {
		m.getRefrescos().add(new Refresco("Coca Cola", 0.90, 50));
		m.getRefrescos().add(new Refresco("Monster", 1.50, 50));
		m.getRefrescos().add(new Refresco("Agua", 0.50, 50));
		m.getRefrescos().add(new Refresco("Fanta Naranja", 0.90, 50));
		m.getRefrescos().add(new Refresco("Fanta Limón", 0.90, 50));
		m.getRefrescos().add(new Refresco("Batido Vainilla", 1, 50));
		m.getRefrescos().add(new Refresco("Batido Chocolate", 1, 50));
		m.getRefrescos().add(new Refresco("Zumo naranja", 0.70, 50));
		m.getRefrescos().add(new Refresco("Zumo manzana", 0.70, 50));
		m.getRefrescos().add(new Refresco("Red Bull", 1.50, 50));
	}
	
	public static void consultarPrecio() {
		Scanner sc = new Scanner(System.in);
		int op = 0;
		
		for(int i = 0; i < m.getRefrescos().size(); i++) {
			System.out.println((i + 1) + " - " + m.getRefrescos().get(i).getNombre());
		}
		do {
			try {
				op = sc.nextInt();
			} catch(Exception e) {
				sc.nextLine();
			}
			if(!(op > 0 && op < m.getRefrescos().size())) {
				System.out.println("Opción incorrecta, vuelva a introducir una opción.");
			}
		} while(!(op > 0 && op < (m.getRefrescos().size() + 1)));
		System.out.println("\nEl precio de la " + m.getRefrescos().get(op - 1).getNombre() + " es de " + m.getRefrescos().get(op - 1).getPrecio() + "€.\n");
	}
	
	public static void comprarRefresco() {
		Scanner sc = new Scanner(System.in);
		sc.useLocale(Locale.ENGLISH);
		Refresco r;
		double resto = 0;
		double dineroIntroducido = 0;
		double dinero = 0;

		do {		
			try {
					System.out.println("Introduzca el dinero...");
					dineroIntroducido = sc.nextDouble();
			} catch(Exception e) {
				System.out.println("Error.");
				sc.nextLine();
			}
		} while (dineroIntroducido <= 0);
		
		System.out.println("Elija el refresco que quiere comprar: ");
		for(int i = 0; i < m.getRefrescos().size(); i++) {
			System.out.println((i + 1) + " - " + m.getRefrescos().get(i).getNombre());
		}
		r = m.getRefrescos().get((sc.nextInt() - 1));
		
		resto = dineroIntroducido - r.getPrecio();
		
		while (resto < 0) {
			System.out.println("Faltan " + (resto * -1) + "€. Introduzca dinero");
			do {		
				try {
						System.out.println("Introduzca el dinero...");
						dinero = sc.nextDouble();
				} catch(Exception e) {
					System.out.println("Error.");
					sc.nextLine();
				}
				dineroIntroducido += dinero;
			} while (dineroIntroducido <= 0);
			resto = dineroIntroducido - r.getPrecio();
		}
		
		if (r.getStock() > 0) {
			System.out.println(r.getNombre() + " entregada.");
			r.buyRefresco();
			if (resto > 0) {
				System.out.println("Devolviendo " + resto + "€.");
			}
		} else {
			System.out.println("No hay suficientes " + r.getNombre() + "s. Devolviendo " + resto + "€.");
		}
	}

}
