package com.mybank.modelo;

import com.mybank.exceptions.OverdraftException;

public class Account {
	protected double balance;
	
    protected Account(double initBalance){
        if(initBalance > 0){
            this.balance = initBalance;
        }
    }

	public double getBalance() {
		return balance;
	}
    
    public void deposit(double amt) {    	
    	if(amt > 0) {
    		this.balance += amt;
    	}
    }
    
    public void withdraw(double amt) throws OverdraftException{    	
    	if((amt > 0) && ((this.balance - amt) > 0)) {
    		this.balance -= amt;
    	} else {
    		throw new OverdraftException("Fondos insuficientes", amt - this.balance);
    	}
    }
    
}
