package com.mybank.modelo;

import java.util.List;

public class Customer {
	private String firstName;
	private String lastName;
	private List<Account> accounts;
	
	public Customer(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Account getAccount(int i) {
		return accounts.get(i);
	}

	public void addAccount(Account account) {
		this.accounts.add(account);
	}
	
	public int getNumOfAccounts() {
		return accounts.size();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
}
