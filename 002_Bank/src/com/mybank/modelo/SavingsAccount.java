package com.mybank.modelo;

public class SavingsAccount extends Account {
	private double interestRate;

	public SavingsAccount(double initBalance, double interestRate) {
		super(initBalance);
		this.interestRate = interestRate;
	}

	public double getInterestRate() {
		return interestRate;
	}
	
}
