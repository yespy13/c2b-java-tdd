package com.mybank.modelo;

import com.mybank.exceptions.OverdraftException;

public class CheckingAccount extends Account {
	private double overdraftAmount;

	public CheckingAccount(double initBalance, double overdraft) {
		super(initBalance);
		this.overdraftAmount = overdraft;
	}

	public CheckingAccount(double initBalance) {
		super(initBalance);
		this.overdraftAmount = 0.0;
	}
    
    public void withdraw(double amt) throws OverdraftException {
    	double neededOverdraft = 0;
    	
    	if(this.balance < amt) {
    		neededOverdraft = amt - this.balance;
    	}
    	
    	if((overdraftAmount > neededOverdraft) && (this.balance - amt < 0)) {
    		this.balance = 0;
    		this.overdraftAmount -= neededOverdraft; 
    	} else if (this.balance - amt >= 0) {
    		this.balance -= amt;
    	} else {
    		throw new OverdraftException("Transaction failed.", neededOverdraft);
    	}
    }

	public double getOverdraftAmount() {
		return overdraftAmount;
	}
    
    
	
}
