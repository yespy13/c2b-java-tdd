package com.mybank.modelo;

import java.util.ArrayList;
import java.util.List;

public class Bank {
	private static List<Customer> customers = new ArrayList<Customer>();;
	private static int numberOfCustomers;	
	
	public Bank() {
		super();
		customers = new ArrayList<Customer>();
		numberOfCustomers = 0;
	}

	public static int getNumOfCustomers() {
		return numberOfCustomers;
	}
	
	public static void addCustomer(String firstName, String lastName) {
		Customer c = new Customer(firstName, lastName);
		customers.add(c);
		numberOfCustomers++;
	}
	
	public void removeCustomer(String firstName, String lastName) {
		if(customers.size() > 0) {
			for (int i = 0; i < this.numberOfCustomers; i++) {
				if (getCustomer(i).getFirstName() == firstName && getCustomer(i).getLastName() == lastName) {
					customers.remove(i);
				}
			}
		}
		//this.customers[this.numberOfCustomers] = new Customer(firstName, lastName);
		this.numberOfCustomers--;
	}
	
	public static Customer getCustomer(int i) {
		return customers.get(i);
	}
}
