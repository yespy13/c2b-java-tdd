package com.mybank.pruebas;

import com.mybank.exceptions.OverdraftException;
import com.mybank.modelo.CheckingAccount;
import com.mybank.modelo.Customer;
import com.mybank.modelo.SavingsAccount;

public class TestBanking {

	public static void main(String[] args) {
		
		/* EJERCICIO 1 */
		
		/*Customer c1 = new Customer("Luis", "Ramos");
		System.out.println("Creando el cliente " + c1.getFirstName() + " " + c1.getLastName());
		Account a1 = new Account(500);
		System.out.println("Creando su cuenta bancaria con saldo " + a1.getBalance());
		
		c1.addAccount(a1);
		
		System.out.println("Sacar 150.00: " + a1.withdraw(150));
		System.out.println("Ingresar 22.50: " + a1.deposit(22.5));
		System.out.println("Sacar 47.62: " + a1.withdraw(47.62));
		System.out.println("Sacar 400.00: " + a1.withdraw(400));
		System.out.println("El cliente " + c1.getFirstName() + " " + c1.getLastName() + " tiene un saldo de " + a1.getBalance() + "�.");*/
		
		/* EJERCICIO 2 */
		
		/*Bank b = new Bank();

		b.addCustomer("Jane", "Sims");
		b.addCustomer("Owen", "Bryant");
		b.addCustomer("Tim", "Soley");
		b.addCustomer("Maria", "Soley");
		
		for(int i = 1; i <= b.getNumOfCustomers(); i++) {
			System.out.println("Cliente [" + i + "] es " + b.getCustomer(i-1).getLastName() + ", " + b.getCustomer(i-1).getFirstName() + ".");
		}
		
		System.out.println("------");
		
		b.removeCustomer("Owen", "Bryant");
		
		for(int i = 1; i <= b.getNumOfCustomers(); i++) {
			System.out.println("Cliente [" + i + "] es " + b.getCustomer(i-1).getLastName() + ", " + b.getCustomer(i-1).getFirstName() + ".");
		} */
		
		/* EJERCICIO 3 */
		
		/*Customer c1 = new Customer("Jane", "Smith");
		System.out.println("Creando el cliente " + c1.getFirstName() + " " + c1.getLastName());
		SavingsAccount sa1 = new SavingsAccount(500, 3);
		c1.addAccount(sa1);
		System.out.println("Creando su cuenta bancaria con saldo " + sa1.getBalance() + " y " + sa1.getInterestRate() + " de interés.");
		Customer c2 =  new Customer("Owen", "Bryant");
		System.out.println("Creando el cliente " + c2.getFirstName() + " " + c2.getLastName());
		CheckingAccount ca1 = new CheckingAccount(500, 0);
		c2.addAccount(ca1);
		System.out.println("Creando su cuenta bancaria con saldo " + ca1.getBalance() + " y " + ca1.getOverdraftAmount() + " de overdraft.");
		Customer c3 = new Customer("Tim", "Soley");
		System.out.println("Creando el cliente " + c3.getFirstName() + " " + c3.getLastName());
		CheckingAccount ca2 = new CheckingAccount(500, 500);
		c3.addAccount(ca2);
		System.out.println("Creando su cuenta bancaria con saldo " + ca2.getBalance() + " y " + ca2.getOverdraftAmount() + " de overdraft.");
		Customer c4 = new Customer("Maria", "Soley");
		System.out.println("Creando el cliente " + c4.getFirstName() + " " + c4.getLastName());
		c4.addAccount(ca2);
		System.out.println(c4.getFirstName() + " comparte cuenta con su marido " + c3.getFirstName());
		
		System.out.println("\n--- MOVIMIENTOS ---\n");
		
		System.out.println("Movimientos de la clienta " + c1.getFirstName() + " " + c1.getLastName());
		System.out.println("Sacamos 150€: ");
		System.out.println("Metemos 22.5€: ");
		System.out.println("Sacamos 47.62€: ");
		System.out.println("Sacamos 400€: ");
		try {
			c1.getAccount().withdraw(150);
			c1.getAccount().deposit(22.5);
			c1.getAccount().withdraw(47.62);
			c1.getAccount().withdraw(400);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("El cliente [" + c1.getLastName() + ", " + c1.getFirstName() + "] tiene un balance de " + c1.getAccount().getBalance() + "€");
		
		System.out.println();
		
		System.out.println("Movimientos del cliente" + c2.getFirstName() + " " + c2.getLastName());
		System.out.println("Sacamos 150€: ");
		try {
			c2.getAccount().withdraw(150);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Metemos 22.5€: ");
		c2.getAccount().deposit(22.5);
		System.out.println("Sacamos 47.62€: ");
		try {
			c2.getAccount().withdraw(47.62);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Sacamos 400€: ");
		try {
			c2.getAccount().withdraw(400);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("El cliente [" + c2.getLastName() + ", " + c2.getFirstName() + "] tiene un balance de " + c2.getAccount().getBalance() + "€");
		
		System.out.println();
		
		System.out.println("Movimientos del cliente" + c3.getFirstName() + " " + c3.getLastName());
		System.out.println("Sacamos 150€: ");
		try {
			c3.getAccount().withdraw(150);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Metemos 22.5€: ");
		c3.getAccount().deposit(22.5);
		System.out.println("Sacamos 47.62€: ");
		try {
			c3.getAccount().withdraw(47.62);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Sacamos 400€: ");
		try {
			c3.getAccount().withdraw(400);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("El cliente [" + c3.getLastName() + ", " + c3.getFirstName() + "] tiene un balance de " + c3.getAccount().getBalance() + "€");
		
		System.out.println();
		
		System.out.println("Movimientos de la clienta " + c4.getFirstName() + " " + c4.getLastName() + "en su cuenta compartida con su marido " + c3.getFirstName() + ".");
		System.out.println("Metemos 150€: ");
		c4.getAccount().deposit(150);
		System.out.println("Sacamos 750€: ");
		try {
			c4.getAccount().withdraw(750);
		} catch (OverdraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("El cliente [" + c4.getLastName() + ", " + c4.getFirstName() + "] tiene un balance de " + c4.getAccount().getBalance() + "€");*/
	}

}
