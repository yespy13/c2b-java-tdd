package com.mybank.data;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

import com.mybank.modelo.*;

public class DataSource {
	private File dataFile;

	public DataSource(String dataFilePath) {
		super();
		this.dataFile = new File(dataFilePath);
	}
	
	public void loadData() throws IOException {
		Scanner input = new Scanner(dataFile);
		input.useLocale(Locale.ENGLISH);
		Customer customer;
		int numOfCustomers = input.nextInt();
		
		for(int i = 0; i < numOfCustomers; i++) {
			String firstName = input.next();
			String lastName = input.next();
			Bank.addCustomer(firstName, lastName);
			customer = Bank.getCustomer(i);
			int numOfAccounts = input.nextInt();
			while(numOfAccounts --> 0) {
				char accountType = input.next().charAt(0);
				float initBalance;
				switch (accountType) {
					case 'S':
						initBalance = input.nextFloat();
						float interesatRate = input.nextFloat();
						customer.addAccount(new SavingsAccount(initBalance, interesatRate));
						break;
					case 'C':
						initBalance = input.nextFloat();
						float overdraftProtection = input.nextFloat();
						customer.addAccount(new CheckingAccount(initBalance, overdraftProtection));
						break;
				}
			}
		}
	}
}
