package maquinaexpendedora;

public class PruebasMaquina {

	public static void main(String[] args) {
		MaquinaExpendedora m = new MaquinaExpendedora();
		System.out.println(m.display());
		
		try {
			m.vender(TiposRefresco.MONSTER, 20);
			System.out.println("Vendiendo");
		} catch(VentaRefrescoException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			m.vender(TiposRefresco.COCA_COLA, 20);
			System.out.println("Vendiendo");
		} catch(VentaRefrescoException e) {
			System.out.println(e.getMessage());
		}
		
		double cambios = m.vender(TiposRefresco.COCA_COLA, 400);
	}

}
