package maquinaexpendedora;

public class Refresco {
	public static final int NUM_MAX_REFRESCOS = 15;
	private TiposRefresco tipo;
	private double precio;
	private int stock;
	
	public Refresco(TiposRefresco tipo) {
		super();
		this.tipo = tipo;
		this.precio = tipo.getCoste()*2;
		this.stock = NUM_MAX_REFRESCOS;
	}

	public TiposRefresco getTipo() {
		return tipo;
	}

	public double getPrecio() {
		return precio;
	}

	public int getStock() {
		return stock;
	}

	@Override
	public String toString() {
		return "Refresco [tipo=" + tipo + ", precio=" + precio + ", stock=" + stock + "]";
	}
	
	public void sacarRefresco() throws VentaRefrescoException {
		if(this.stock == 0) {
			throw new VentaRefrescoException("No hay stock de " + this.tipo.getNombre());
		}
		this.stock--;
	}
}
