package maquinaexpendedora;

import java.util.HashMap;
import java.util.Map;

public class MaquinaExpendedora {
	private Map<TiposRefresco, Refresco> casillerosRefrescos;
	private double cambios;
	private double recaudacion;
	
	public MaquinaExpendedora() {
		this.casillerosRefrescos = new HashMap<TiposRefresco, Refresco>();
		this.casillerosRefrescos.put(TiposRefresco.AQUARIUS, new Refresco(TiposRefresco.AQUARIUS));
		this.casillerosRefrescos.put(TiposRefresco.MONSTER, new Refresco(TiposRefresco.MONSTER));
		this.casillerosRefrescos.put(TiposRefresco.COCA_COLA, new Refresco(TiposRefresco.COCA_COLA));
		this.casillerosRefrescos.put(TiposRefresco.FANTA_NARANJA, new Refresco(TiposRefresco.FANTA_NARANJA));
		this.casillerosRefrescos.put(TiposRefresco.NESTEA, new Refresco(TiposRefresco.NESTEA));
		
		this.cambios = 100;
	}
	
	public String display() {
		StringBuilder sb = new StringBuilder();
		for(Refresco r : casillerosRefrescos.values()) {
			sb.append(". " + r + "\n");
		}
		sb.append(". Cambio disponible: " + this.cambios + "€.");
		return sb.toString();
	}
	
	public double vender(TiposRefresco tipo, double dinero) throws VentaRefrescoException {
		if(!this.casillerosRefrescos.containsKey(tipo)) {
			throw new VentaRefrescoException("No hay refrescos de este tipo.");
		}
		
		if(dinero <= 0) {
			throw new VentaRefrescoException("Debe introducir un importe válido");
		}
		
		this.casillerosRefrescos.get(tipo).sacarRefresco();
		this.recaudacion += this.casillerosRefrescos.get(tipo).getPrecio();
		
		return dinero - this.casillerosRefrescos.get(tipo).getPrecio();
	}
}
