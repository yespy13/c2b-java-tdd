package com.curso.string;

public class EjemploString {

	public static void main(String[] args) {
		String nombre = "Luis";
		String nombre2 = "Luis";
		String nombre3 = new String("Luis");
		
		if(nombre == nombre2) {
			System.out.println("Son iguales.");
		} else {
			System.out.println("No son iguales.");
		}
		
		if(nombre == nombre3) {
			System.out.println("Son iguales.");
		} else {
			System.out.println("Nombre3 es distinto.");
		}
		
		nombre = nombre.toUpperCase();
		nombre = nombre.concat(" Ramos");
		System.out.println(nombre);
		
		StringBuilder sb;
		StringBuffer sbf;
		
		sb = new StringBuilder("Luis");
		sb.append(" Ramos");
		
		System.out.println(sb);
	}

}
