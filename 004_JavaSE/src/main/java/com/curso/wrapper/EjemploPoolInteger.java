package com.curso.wrapper;

public class EjemploPoolInteger {

	public static void main(String[] args) {
		int a = 2;
		int b = 2;
		
		System.out.println("a == b ==> " + (a == b));
		
		Integer a1 = 332;
		Integer b1 = 332;
		Integer c1 = new Integer(332);

		System.out.println("a1 == b1 ==> " + (a1 == b1));
		System.out.println("a1 equals b1 ==> " + (a1.equals(b1)));
		System.out.println("a1 == c1 ==> " + (a1 == c1));
		
		if(a1 == 332) {
			System.out.println("Todo bien A");
		}
		
		if(a1.equals(332)) {
			System.out.println("Tengo sueño");
		}
	}

}
