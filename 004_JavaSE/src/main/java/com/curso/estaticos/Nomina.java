package com.curso.estaticos;

public class Nomina {
	private static final double IRPF_MINIMO = 2.4;
	private static double salario;
	
	public static double calcularIRPF() {
		double irpf = salario * (IRPF_MINIMO/100);
		return irpf;
	}
	
	public static void imprimirNomina() {
		System.out.println("Salario: " + salario);
		System.out.println("IRPF: " + calcularIRPF());
	}
}
