package com.curso.estaticos;

public class Contador4 {
	private static int contador;
	private int serialNumber;
	
	static {
		contador = 10;
	}
	
	public Contador4(int serialNumber) {
		super();
		contador++;
		this.serialNumber = contador;
	}

	public static int getContador() {
		return contador;
	}

	public int getSerialNumber() {
		return serialNumber;
	}
}
