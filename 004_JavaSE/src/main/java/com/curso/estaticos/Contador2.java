package com.curso.estaticos;

public class Contador2 {
	private static int contador = 0;
	private int serialNumber;
	
	public Contador2(int serialNumber) {
		super();
		contador++;
		this.serialNumber = contador;
	}

	public static int getContador() {
		return contador;
	}

	public int getSerialNumber() {
		return serialNumber;
	}
	
	
	
}
