package com.curso.enumeraciones;

public class Juego {

	public static void main(String[] args) {
		/*Palos p = Palos.BASTOS;
		
		if(p == Palos.OROS) {
			System.out.println("Son oros.");
		}
		
		System.out.println("Name: " + p.name());
		System.out.println("Ordinal: " + p.ordinal());
		System.out.println("Valores: " + p.values());
		
		for(Palos pa : p.values()) {
			System.out.println(pa);
		}
		
		System.out.println(p.values()[2]);*/
		
		Carta c1 = new Carta();
		System.out.println(c1);
		Carta c2 = new Carta();
		System.out.println(c2);
		checkWinner(jugar(c1, c2));
	}

	private static int jugar(Carta c1, Carta c2) {
		if(c1.getRango() > c2.getRango()) {
			return 1;
		} else if (c1.getRango() < c2.getRango()) {
			return 2;
		} else {
			return checkEmpate(c1, c2);
		}
	}

	private static int checkEmpate(Carta c1, Carta c2) {
		if(c1.getPalo().getValor() > c2.getPalo().getValor()) {
			return 1;
		} else if(c1.getPalo().getValor() < c2.getPalo().getValor()) {
			return 2;
		} else {
			return 0;
		}		
	}

	private static void checkWinner(int winner) {
		if(winner == 1) {
			System.out.println("Gana el jugador 1.");
		} else if (winner == 2) {
			System.out.println("Gana el jugador 2.");
		} else {
			System.out.println("Empate.");
		}
	}

}
