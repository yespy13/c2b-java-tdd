package com.curso.enumeraciones;

import java.util.Random;

public class Carta {
	private Palos palo;
	private int rango;
	
	public Carta() {
		super();
		this.palo = palo.values()[new Random().nextInt(4)];
		this.rango = (new Random().nextInt(10)) + 1;
	}

	public Palos getPalo() {
		return palo;
	}

	public int getRango() {
		return rango;
	}

	@Override
	public String toString() {
		return "Carta [palo=" + palo + ", rango=" + rango + "]";
	}
	
}
