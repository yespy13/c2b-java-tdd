package com.curso.enumeraciones;

public enum Palos {
	OROS(4), COPAS(3), BASTOS(1), ESPADAS(2);
	
	private final int valor;
	
	private Palos(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return valor;
	}
}
