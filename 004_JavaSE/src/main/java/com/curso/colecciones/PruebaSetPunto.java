package com.curso.colecciones;

import java.util.*;

public class PruebaSetPunto {
	public static void main(String[] args) {
		Set<Punto> puntos = new TreeSet<Punto>(new OrdenarPuntoAlReves());

		puntos.add(new Punto(2, 3));
		puntos.add(new Punto(5, 3));
		puntos.add(new Punto(6, 1));
		puntos.add(new Punto(1, 7));
		puntos.add(new Punto(2, 3));
		puntos.add(new Punto(5, 5));
		puntos.add(new Punto(8, 2));
		
		for(Punto p : puntos) {
			System.out.println(p.toString());
		}
	}
}
