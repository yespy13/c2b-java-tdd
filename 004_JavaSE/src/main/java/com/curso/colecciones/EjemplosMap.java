package com.curso.colecciones;

import java.util.*;

public class EjemplosMap {
	public static void main(String[] args) {
		Map<Integer, Empleado> map = new HashMap();
		map.put(123, new Empleado(123, "Luis Ramos", "1234A"));
		map.put(552, new Empleado(552, "Sara Ndonga", "1897C"));
		map.put(711, new Empleado(711, "Tor Quemada", "3278Y"));
		map.put(143, new Empleado(143, "Mar Cos", "5641W"));
		
		Empleado e = map.get(143);
		System.out.println(e);

		map.put(143, new Empleado(143, "Ragh Narok", "1175L"));
		
		e = map.get(143);
		System.out.println(e);		
		
		Map<Punto, String> figura = new TreeMap();
		figura.put(new Punto(5, 0), "Red");
		figura.put(new Punto(2, 3), "Blue");
		figura.put(new Punto(3, 2), "Green");
		
		Set<Punto> claves = figura.keySet();
		for(Punto p : claves) {
			System.out.println(p + " color " + figura.get(p));
		}
	}
}
