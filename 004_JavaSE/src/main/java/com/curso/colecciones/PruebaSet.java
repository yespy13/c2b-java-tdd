package com.curso.colecciones;

import java.util.*;

import com.curso.interfaces.ObjetoVolador;

public class PruebaSet {
	public static void main(String[] args) {		
		Integer i = new Integer(2);
		Integer ii = 2;
		
		Set<Integer> notas = new TreeSet<Integer>();
		
		notas.add(1);
		notas.add(7);
		notas.add(2);
		notas.add(8);
		notas.add(2);
		notas.add(9);
		
		for(Integer n : notas) {
			System.out.println(n);
		}
	}
	
	public void aterrizaje(Set<ObjetoVolador> volables) {
		
	}
}
