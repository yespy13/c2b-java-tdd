package com.curso.colecciones;

public class PruebasEquals {

	public static void main(String[] args) {
		Punto p1 = new Punto(1, 2);
		Punto p2 = new Punto(1, 2);
		Punto p3 = p1;
		
		System.out.println("p1 == p2 ==>" + (p1 == p2));
		System.out.println("p1.equals(p2) ==> " + (p1.equals(p2)));
	}

}
