package com.curso.excepciones;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class EjemplosTryCatchFinally {
	public static void main(String[] args) {
		try {
			File f = new File("cartas.txt");
			f.createNewFile();
		} catch(FileNotFoundException e) {
			System.out.println("Fichero no encontrado.");
		} catch(IOException e) {
			System.out.println("Error al escribir o leer.");
		} catch (Exception e) {
			System.out.println("Error inesperado.");
			e.printStackTrace();
		} finally {
			System.out.println("Siempre me ejecuto, ocurra o no la excepción.");
			System.out.println("Incluso si haces un return o un 'throw new Exception'");
		}
		
		System.out.println("Fin");
	}
	
	public void calculaNomina(String id, double categoria) {
		try {
			/*if(id == null || service.getEmpleadoPorId() == null) {
				throw new NominaException("ID no existe.");
			}*/
			
		} finally {
			
		}
	}
}
