package com.curso.excepciones;

public class EjemplosRuntimeException {
	public static void main(String[] args) {
		String numero = "2";
		
		int numeroInt = Integer.parseInt(numero);
		
		System.out.println("El número es " + numeroInt);
		
		String nombre = null;
		
		if(nombre != null)
			System.out.println("Nombre en mayúsculas: " + nombre.toUpperCase());
	}
}
