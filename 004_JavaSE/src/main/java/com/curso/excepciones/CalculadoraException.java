package com.curso.excepciones;

public class CalculadoraException extends Exception {	
	private boolean denominadorMal = false;

	public CalculadoraException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalculadoraException(String message, boolean denominadorMal) {
		super(message);
		this.denominadorMal = denominadorMal;
	}
	
	public boolean isDenominadorMal() {
		return denominadorMal;
	}
}
