package com.curso.excepciones;

public class GenerarNominasService {

	public static void main(String[] args) {
		GenerarNominasService service = new GenerarNominasService();
		
		try {
			service.generarNomina(170020, 14);
			service.generarNomina(17500, 0);
		} catch (CalculadoraException ce) {
			if(ce.isDenominadorMal()) {
				System.out.println("No pudo dividir, denominador mal.");
			} else {
				System.out.println("No pudo dividir, numerador mal.");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void generarNomina(double salario, int meses) throws Exception {
		System.out.println("Nómina es " + Calculadora.divide(salario, meses));
	}

}
