package com.curso.excepciones;

import java.io.File;
import java.io.IOException;

public class EjemplosExcepcionesChecked {

	public static void main(String[] args) {
		File fichero = new File("carta.txt");
		try {
			fichero.createNewFile();
			System.out.println("Fichero creado");
		} catch (IOException e) {
			System.out.println("No se pudo crear el fichero " + e.getMessage());
			e.printStackTrace();
		}
	}

}
