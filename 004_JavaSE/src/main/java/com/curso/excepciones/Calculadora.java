package com.curso.excepciones;

public class Calculadora {
	public static double divide(double salario, int n2) throws Exception {
		double r = 0;
		
		try {
			if (n2 <= 0 ) {
				throw new CalculadoraException("No puede dividir, el divisor debe ser positivo.", false);
			}
		} catch (CalculadoraException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		r = salario/n2;
		return r;
	}
}
