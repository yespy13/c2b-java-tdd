package com.curso.interfaces;

public interface InterfaceNuevoJava8 {
	public static void saluda(String nombre) {
		System.out.println("Hola interfaz Java 8: " + nombre);
	}
	
	public default void print() {
		System.out.println("TestInterface :: Default method");
	}
}
