package com.curso.interfaces;

public class Superman implements ObjetoVolador {

	@Override
	public void volar() {
		System.out.println("Vuelo porque soy de Krypton.");
	}

	@Override
	public void aterrizar() {
		System.out.println("Superhero landing!");
	}

	@Override
	public void despegar() {
		System.out.println("¡A volar, Krypton!");
	}

}
