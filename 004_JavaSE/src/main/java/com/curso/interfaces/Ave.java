package com.curso.interfaces;

public class Ave extends Animal implements ObjetoVolador {

	@Override
	public void volar() {
		System.out.println("Vuelo porque soy un ave.");
	}

	@Override
	public void aterrizar() {
		System.out.println("Aterrizo porque soy un ave.");
	}

	@Override
	public void despegar() {
		System.out.println("Despego como un ave, pero no un avestruz.");
	}

	@Override
	public void solicitarViaje() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void comer() {
		System.out.println("Necesito comer para vivir, porque soy un animal :(");
	}

}
