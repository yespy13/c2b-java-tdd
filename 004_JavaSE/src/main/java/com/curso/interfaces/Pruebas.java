package com.curso.interfaces;

public class Pruebas {

	public static void main(String[] args) {
		Animal a = new Ave();
		ObjetoVolador b = new Superman();
		
		b.volar();
		hacerVolar(new Ave(), new Superman(), b);
		
	}
	
	public static void hacerVolar(ObjetoVolador... voladores) {
		for(ObjetoVolador ov : voladores) {
			if(ov instanceof Ave) {
				((Ave)ov).comer();
			}
			ov.volar();
		}
	}
}
