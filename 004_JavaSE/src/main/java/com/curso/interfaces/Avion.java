package com.curso.interfaces;

public class Avion implements Transportable {

	@Override
	public void volar() {
		System.out.println("Vuelo con mis motores de avión.");
	}

	@Override
	public void aterrizar() {
		System.out.println("Aterrizo con mis ruedas, todo bien.");
	}

	@Override
	public void despegar() {
		System.out.println("Despego a toda hostia para que mis motores vayan a tope.");
	}

	@Override
	public void transportar() {
		System.out.println("Transporto mercancía o personas.");
	}

	@Override
	public void solicitarViaje() {
		System.out.println("Puedes solicitar viajes en mí, chuchu... Ah no, eso es para el tren, ¿fium?");
	}

}
