package com.curso.interfaces;

public interface ObjetoVolador {
	public static final double ALTURA_MAXIMA_VUELO = 200.9;
	
	/** Todos son public y abstract, se ponga o no. **/
	
	public abstract void volar();
	public void aterrizar();
	void despegar();
}
