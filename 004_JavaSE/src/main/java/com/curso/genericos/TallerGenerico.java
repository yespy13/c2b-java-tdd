package com.curso.genericos;

public class TallerGenerico<E extends Vehiculo> {
	public void reparar(E vehiculo) {
		vehiculo.verMotor();
	}
	
	public static void main(String[] args) {
		TallerGenerico<Coche> taller = new TallerGenerico<Coche>();
		taller.reparar(new Coche());
	}
}

abstract class Vehiculo {
	public abstract void verMotor();
}

class Coche extends Vehiculo {

	@Override
	public void verMotor() {
		System.out.println("Motor diesel.");
	}
	
	public void tocarClaxon() {
		System.out.println("Moooooooc");
	}
}