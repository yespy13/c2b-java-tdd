package com.curso.io.scanner;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.Scanner;

public class EjemploScanner {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		Locale italia = new Locale("IT");
		Locale canada = new Locale("FR", "CA");
		
		//sc.useLocale(canada);
		//sc.useDelimiter("#");
		
		/*System.out.println("Introduce dato: ");
		String dato = sc.next();
		System.out.println("Has puesto " + dato );*/
		
		/*String cadenaDatos = "1 Begoña 30,9";
		
		Scanner sc2  = new Scanner(cadenaDatos);
		int id = sc2.nextInt();
		String nombre = sc2.next();
		double peso = sc2.nextDouble();
		System.out.printf("%d %s %5.2f", id, nombre, peso);*/
		
		try {
			Scanner scf = new Scanner(new FileReader("textos.properties"));
			
			scf.useDelimiter("\n");
			while(scf.hasNext())
				System.out.println(scf.next());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
