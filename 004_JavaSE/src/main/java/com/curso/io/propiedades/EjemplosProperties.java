package com.curso.io.propiedades;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class EjemplosProperties {
	public static void main(String[] args) {
		Properties sistema = System.getProperties();
		
		//
		
		Properties textos = new Properties();
		System.out.println(textos.get("titulo"));
		try {
			textos.load(new FileReader("textos.properties"));
			textos.list(System.out);
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado.");
		} catch (IOException e) {
			System.out.println("Error al leer.");
		}
	}
}
