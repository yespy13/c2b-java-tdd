package com.curso.io.ficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class EjemplosFiles {
	public static void main(String[] args) throws IOException {
		File dir = new File("correspondencia");
		dir.mkdir();
		
		File carta = new File(dir, "carta2.txt");
		carta.createNewFile();
		
		PrintWriter out = new PrintWriter(new FileWriter(carta));
		out.println("Querido amigo: ");
		out.println("Espero que a la recepción de esta carta sigas bien.");
		
		out.close();
		
		BufferedReader in = new BufferedReader(new FileReader(carta));
		
		String linea = "";
		while(linea != null) {
			linea = in.readLine();
			System.out.println(linea);
		}
	}
}
